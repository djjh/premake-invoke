-- invoke.lua
-- The invoke Premake Module.

local m = {}

m._VERSION = "0.0.0"

local function get_command_line_arguments(opts)
    local args = {}
    for k,v in pairs(table.merge(_OPTIONS, opts)) do
        if type(k) ~= "number" then
            table.insert(args, ("--%s=%s"):format(k,tostring(v)))
        end
    end
    return table.join(args, _ARGS, opts)
end

function m.invoke(fmt, ...)
    local cmd = (string.format(fmt or "", select(1, ...)))
    local isQuiet = cmd:contains("quiet") or _OPTIONS.quiet

    --[[
    local output, code = os.outputof(cmd)
    if not isQuiet then
        print(output)
    end
    --]]

    if os.ishost("windows") then
        return os.executef("%s %s", cmd, isQuiet and "> NUL 2>&1" or "")
    else
        return os.executef("%s %s", cmd, isQuiet and ">/dev/null 2>&1" or "")
    end
end

function m.reinvoke(action, opts)
    if type(action) == "table" then
        opts = action
        action = nil
    end
    local cmd = string.format("premake5 %s %s", action or _ACTION or "", table.concat(get_command_line_arguments(opts), " "))
    return m.invoke(cmd)
end

function m.reinvoke_env(env, action, opts)
    if type(action) == "table" then
        opts = action
        action = nil
    end
    local cmd = string.format("premake5 %s %s", action or _ACTION or "", table.concat(get_command_line_arguments(opts), " "))
    --print_debug(cmd)
    return m.invoke("%s && %s", env, cmd)
end

function m.set_default_action(action)
    if action and not _ACTION then
        _ACTION = action
    end
end

return m
